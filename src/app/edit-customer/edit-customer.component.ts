import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { CustomerService } from '../customer.service';
import { ActivatedRoute } from '@angular/router';
import { Customer } from '../customer/customer';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.scss']
})
export class EditCustomerComponent implements OnInit {
  public customer: Customer;

  constructor(private appComponent: AppComponent, private customerService: CustomerService, private route: ActivatedRoute) {
    this.appComponent.setTitle('Editar cliente');
  }

  ngOnInit() {
    this.getCustomer();
  }

  getCustomer() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.customer = this.customerService.getCustomer(id);
  }

}
