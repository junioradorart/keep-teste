import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  sectionTitle = 'Cadastro de clientes';

  setTitle(title: string) {
    this.sectionTitle = title;
  }
}
