import { Injectable, Inject } from '@angular/core';
import { Customer } from './customer/customer';

@Injectable()
export class CustomerService {
  customers: Customer[];

  constructor(@Inject('LOCALSTORAGE') private localStorage: any) {
    this.customers = [];
    // this.createInitialCustomers();
  }

  createInitialCustomers() {
    if (!this.localStorage.getItem('customers')) {

    }

    this.customers.push({
      id: 1,
      name: 'Jose Junior',
      email: 'junioradorart@gmail.com',
      phone: '14 3232-3232',
      cel: '14 99999-9999',
      password: 'asdf123'
    });

    this.customers.push({
      id: 2,
      name: 'Lorem Ipsum',
      email: 'lorenipsum@gmail.com',
      phone: '14 3232-3232',
      cel: '14 99999-9999',
      password: 'asdf123'
    });

    this.customers.push({
      id: 3,
      name: 'Andre Santos',
      email: 'zandre@gmail.com',
      phone: '14 3536-3232',
      cel: '14 99999-9999',
      password: 'asdf123'
    });

    this.save();
  }

  getCustomer(id: number) {
    let customer = this.getCustomers().filter((customer) => customer.id === id);
    return customer.length ? customer[0] : undefined;
  }

  updateCustomer(customer: Customer) {
    this.customers = this.getCustomers().map((el) => el.id === customer.id ? customer : el);
    this.save();
  }

  getCustomers() {
    let customers = JSON.parse(this.localStorage.getItem('customers'));
    return customers ? customers : [];
  }

  addCustomer(customer: Customer) {
    this.customers = this.getCustomers();
    this.customers.push(customer);
    this.save();
  }

  save() {
    this.localStorage.setItem('customers', JSON.stringify(this.customers));
  }

}
