import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { CustomerService } from '../customer.service';
import { Customer } from '../customer/customer';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.scss']
})
export class CustomersListComponent implements OnInit {
  customers: Customer[];

  constructor(private appComponent: AppComponent, private customerService: CustomerService) {
    this.appComponent.setTitle('Lista de clientes');
    this.customers = customerService.getCustomers();
  }

  ngOnInit() {
  }

  setOrder(order: string) {
    this.orderBy(order);
  }

  orderBy(attr: string) {
    this.customers.sort(function (a, b) {
      if (a[attr] > b[attr]) {
        return 1;
      }
      if (a[attr] < b[attr]) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
  }
}
