import { Component, OnInit, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CustomerService } from '../customer.service';
import { Customer } from '../customer/customer';
import { Router } from '@angular/router';
import { TextMaskModule } from 'angular2-text-mask';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.scss']
})
export class CustomerFormComponent implements OnInit {
  @Input('customer') customer: Customer;
  @Input('editMode') editMode: boolean;
  customers: Customer[];
  phoneValidation: boolean;
  nameValidation: boolean;
  emailValidation: boolean;
  passwordValidation: boolean;
  maskPhone = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  maskCel = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(public customerService: CustomerService, private router: Router) {
    this.customers = customerService.getCustomers();
    if (!this.customer) {
      this.setEmptyCustomer();
    }

    this.phoneValidation = true;
    this.nameValidation = true;
    this.emailValidation = true;
    this.passwordValidation = true;
  }

  ngOnInit() {
  }

  setEmptyCustomer() {
    let newId = this.customers.length ? this.customers[this.customers.length - 1].id + 1 : 1;

    this.customer = {
      id: newId,
      name: "",
      email: "",
      phone: "",
      cel: "",
      password: "",
    };
  }

  onSubmit() {
    this.nameValidation = this.customer.name ? true : false;
    this.emailValidation = this.customer.email ? true : false;
    this.passwordValidation = this.customer.password ? true : false;

    if (!this.nameValidation || !this.emailValidation || !this.passwordValidation) {
      return;
    }

    if (!this.customer.cel && !this.customer.phone) {
      this.phoneValidation = false;
      return;

    } else {
      this.phoneValidation = true;
    }

    if (this.editMode) {
      this.customerService.updateCustomer(this.customer);

    } else {
      this.customerService.addCustomer(this.customer);
    }

    this.router.navigateByUrl("/customers");
	}

}
